/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testTriggerAccountAfterInsert {

	//Test methos for AccountAfterTrigger
    static testMethod void InsertAccount() {
    	List <Account> lstAccounts = new List <Account>(); 
    	Account a = new Account();
    	a.Name = 'Nombre de la cuenta A';
    	lstAccounts.add(a);
    	Account b = new Account();
    	b.Name = 'Nombre de la cuenta B';
    	lstAccounts.add(b);
    	Account c = new Account();
    	c.Name = 'Nombre de la cuenta C';
    	lstAccounts.add(c);
    	insert lstAccounts;
    }
}