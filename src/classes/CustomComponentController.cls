public with sharing class CustomComponentController {

	public String controllerValue;
	
	public void setControllerValue (String s) {
		controllerValue = s.toUpperCase();
	}

	public String getControllerValue() {
    	return controllerValue;
	} 
}