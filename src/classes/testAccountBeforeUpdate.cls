/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testAccountBeforeUpdate {
	@testSetup static void insertAccounts() {
		List<Account> testAccts = new List<Account>();
		for(Integer i=0;i<2;i++) {
			testAccts.add(new Account(Name = 'TestAcct'+i,Active__c='Si'));
		}
		insert testAccts;
		
		List<Opportunity> lstOpp = new List<Opportunity>();
		lstOpp.add(new Opportunity());
		lstOpp[0].Name = 'Hollllll';
		lstOpp[0].AccountId = testAccts[0].Id;
		lstOpp[0].StageName = 'Abierta';
		lstOpp[0].CloseDate = Date.newInstance(2200, 1, 1);
			insert lstOpp;	
		
    }
	
    static testMethod void updateAccounts() {
        // TO DO: implement unit test
        List<Account> testAccts = [SELECT Id,Name FROM Account WHERE Name like 'TestAcct%' LIMIT 5];
        
        for(Account a: testAccts){
        	a.Name = a.Name + ' update';
        }
        
        update testAccts;
        
        testAccts[0].Type = 'Tipo personalizado';
        try{
			update testAccts[0];
        }catch(DMLException e){
			//Error forzado;
			Boolean expectedExceptionThrown =  e.getMessage().contains('No puede cambiar el type si tiene oportunidades abiertas.') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}
    }
    
    
    //TestAfterUpdate
    static testMethod void testAfterUpdate() {
        List<Account> testAccts = [SELECT Id,Name FROM Account WHERE Name like 'TestAcct%' LIMIT 5];
        
        for(Account a: testAccts){
        	a.Active__c = 'NO';
        }
        
        update testAccts;
    }
}