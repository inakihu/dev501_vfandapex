/**
 * Pages
 *		- opptyStep1.page
 *		- opptyStep2.page
 *		- opptyStep3.page
 * Controller
 *		- newOpportunityController.cls
 */
@isTest
private class testOpportunityWizard {
	
	static testMethod void testAll() {
    	
    	PageReference pageStep1 = Page.opptyStep1;
        Test.setCurrentPage(pageStep1);
        
        newOpportunityController controller = new newOpportunityController();
        
		controller.getAccount().Name = 'AAAAAAA';
		controller.getAccount().Site = 'www.salesforce.com';
		
        controller.getContact().FirstName = 'firstname';
        controller.getContact().LastName = 'Lastname';
        controller.getContact().Phone = '677677677';
        
        controller.getOpportunity().Name = '';
        
        controller.cancel();
        controller.step2();
        
        PageReference pageStep2 = Page.opptyStep2;
        Test.setCurrentPage(pageStep2);
        
        controller.getOpportunity().Name = 'Name Opp';
        controller.getOpportunity().amount = 10000;
        controller.getOpportunity().Closedate = Date.newInstance(2100,1,1);
		controller.getOpportunity().StageName = 'Stage';
		
		controller.getRole().role = 'role';
		
		controller.cancel();
        controller.step1();
        controller.step3();
        
        PageReference pageStep3 = Page.opptyStep3;
        Test.setCurrentPage(pageStep3);
        
        controller.cancel();
        controller.step2();
        
        controller.save();
        
    }
    
}