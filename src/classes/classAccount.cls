public with sharing class classAccount {
	private Account acc = null;
	
	public classAccount(){
		acc = new Account();	
	}
	
	public Account getAcc(){
		return acc;
	}
	
	public static Integer numberOfAccounts(){
		return [SELECT count() FROM Account];
	}
}