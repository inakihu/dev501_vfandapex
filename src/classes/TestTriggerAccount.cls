/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestTriggerAccount {

    @testSetup static void insertAccounts() {
		List<Account> testAccts = new List<Account>();
		for(Integer i=0;i<5;i++) {
			testAccts.add(new Account(Name = 'TestAcct'+i,Active__c='Si'));
		}
		insert testAccts;
		
		List<Opportunity> lstOpp = new List<Opportunity>();
		lstOpp.add(new Opportunity());
		lstOpp[0].Name = 'Hollllll';
		lstOpp[0].AccountId = testAccts[0].Id;
		lstOpp[0].StageName = 'Abierta';
		lstOpp[0].CloseDate = Date.newInstance(2200, 1, 1);
			insert lstOpp;	
		
    }
     
    //Test method for AccountAfterTrigger
    static testMethod void testAccountAfterTrigger() {
    	List <Account> lstAccounts = new List <Account>(); 
    	Account a = new Account();
    	a.Name = 'Nombre de la cuenta A';
    	lstAccounts.add(a);
    	Account b = new Account();
    	b.Name = 'Nombre de la cuenta B';
    	lstAccounts.add(b);
    	Account c = new Account();
    	c.Name = 'Nombre de la cuenta C';
    	lstAccounts.add(c);
    	insert lstAccounts;
    }
    
    //test method for AccountBeforeUpdate_LAB
    static testMethod void updateAccounts() {
        List<Account> testAccts = [SELECT Id,Name FROM Account WHERE Name like 'TestAcct%' LIMIT 5];
        
        for(Account a: testAccts){
        	a.Name = a.Name + ' update';
        }
                
        update testAccts;
        
        testAccts[0].Type = 'Tipo personalizado';
        try{
			update testAccts[0];
        }catch(DMLException e){
			//Error forzado;
			Boolean expectedExceptionThrown =  e.getMessage().contains('No puede cambiar el type si tiene oportunidades abiertas.') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}
    }
    
    
    //test method for AccountAfterUpdate
    static testMethod void testAfterUpdate() {
        List<Account> testAccts = [SELECT Id,Name FROM Account WHERE Name like 'TestAcct%' LIMIT 5];
        
        for(Account a: testAccts){
        	a.Active__c = 'NO';
        }
        
        update testAccts;
    }
}