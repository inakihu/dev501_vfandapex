public with sharing class AccountImportantFieldSetApex {
	public Account acc { get; set; }
    
    public AccountImportantFieldSetApex() {
        this.acc = getAccount();
	}
    
    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Account.FieldSets.Important.getFields();
	}
    
    private Account getAccount() {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id FROM Account LIMIT 1';
        return Database.query(query);
    }
}