public class IterableString {
	List<String> myString;
	Integer i;
	Integer length {get;set;}

	public IterableString(String param){
		i=0;
		myString = param.split('');
		length = myString.size();
	}
    
    public boolean hasNext(){
       if(i >= length) {
           return false;
       } else {
           return true;
       }
	}
   
    public String next(){
    	return myString[i++];
	}
}