public class ProcessBuilderActions{
	@InvocableVariable
	public String var;
	
    public ID leadId;
    
    @InvocableMethod
    public static void BorrarTareas (List<Id> accountIds)
    {
        List<Task> tasks=[select id from Task where whatId in :accountIds and Subject = 'Validate Account'];
 
        delete tasks;
    }
	
}