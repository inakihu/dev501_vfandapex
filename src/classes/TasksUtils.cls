public class TasksUtils {
    @InvocableMethod(label='Delete Validate Tasks for Account')
    public static void AccountValidated(List<Id> accountIds)
    {
        List<Task> tasks=[select id from Task where whatId in :accountIds and Subject = 'Validate Account'];
 
        delete tasks;
    }
}