trigger AccountAfterUpdate_LAB on Account (after update) {
    //
    List<Account> listNewRecords = Trigger.new;
    List<Account> listOldRecords = Trigger.old;
    
    Set<ID> accountActiveChanged = new Set<ID>();
    for(Integer i=0;i<listNewRecords.size();i++){
        if(listOldRecords[i].Active__c != listNewRecords[i].Active__c && listNewRecords[i].Active__c == 'NO'){
            accountActiveChanged.add(listNewRecords[i].Id);
        }
    }
    
    if(accountActiveChanged.size()>0){
        set<ID> errAccount = new set<ID>();
        List<Opportunity> opps = [Select AccountId FROM Opportunity WHERE isClosed = false AND AccountId IN :accountActiveChanged]; 
        for(Opportunity opp: opps)
            opp.StageName ='Closed Lost';
        update opps;
    }
}