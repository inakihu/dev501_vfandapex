trigger AccountBeforeUpdate_LAB on Account (before update) {
	/* Sin BULK */
	/* List<Account> listNewRecords = Trigger.new;
	List<Account> listOldRecords = Trigger.old;
	if(listOldRecords[0].Type!=listNewRecords[0].Type){
		if([Select count() FROM Opportunity WHERE isClosed = false AND AccountId= :listNewRecords[0].Id LIMIT 1]>0){
			//No permitir guardar
			listNewRecords[0].addError('No puede cambiar el type si tiene oportunidades abiertas');
		}else{
			//Permitir guardar
		}
	}
	*/
	
	/* Con BULK - Query dentro de For error en cuanto se modifique el campo Type en mas de 10 registros*/
	/*List<Account> listNewRecords = Trigger.new;
	List<Account> listOldRecords = Trigger.old;
	for(Integer i=0;i<listNewRecords.size();i++){
		if(listOldRecords[i].Type != listNewRecords[i].Type){
			if([Select count() FROM Opportunity WHERE isClosed = false AND AccountId= :listNewRecords[0].Id LIMIT 1]>0){
				//No permitir guardar
				listNewRecords[i].addError('No puede cambiar el type si tiene oportunidades abiertas');
			}else{
				//Permitir guardar
			}
		}
	}*/
	
	/* Con BULK - Una sola Query */
	List<Account> listNewRecords = Trigger.new;
	Map<ID,Account> mapNewRecords = Trigger.newMap;
	List<Account> listOldRecords = Trigger.old;
	
	Set<ID> accountTypeChanged = new Set<ID>();
	for(Integer i=0;i<listNewRecords.size();i++){
		if(listOldRecords[i].Type != listNewRecords[i].Type){
			accountTypeChanged.add(listNewRecords[i].Id);
		}
	}
	
	if(accountTypeChanged.size()>0){
		
		set<ID> errAccount = new set<ID>();
		for(Opportunity opp: [Select AccountId FROM Opportunity WHERE isClosed = false AND AccountId IN :accountTypeChanged])
			errAccount.add(opp.AccountId);
		
		//Opcion 1	
		/*for(Integer i=0;i<listNewRecords.size();i++){
			if(errAccount.contains(listNewRecords[i].Id)){
				listNewRecords[i].addError('No puede cambiar el type si tiene oportunidades abiertas');
			}
		}*/
		
		//Opcion 2
		for(ID idAcc:errAccount){
			mapNewRecords.get(idAcc).addError('No puede cambiar el type si tiene oportunidades abiertas.');
		}	
	}
}