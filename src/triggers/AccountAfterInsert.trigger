trigger AccountAfterInsert on Account (after insert) {
	// NO Bulk DML
	/*
	for(Account a:Trigger.new){
		Task t = new Task();
		t.Subject = 'Revisa los datos de la cuenta';
		t.WhatId = a.Id;
		Date actualDate = Date.Today();
		actualDate=actualDate.addDays(7);
		t.ActivityDate = actualDate;
		t.OwnerId = a.OwnerId;
		insert t;	
	}
	*/
	List<Task> lstTasks = new List<Task>(); 
	for(Account a:Trigger.new){
		Task t = new Task();
		t.Subject = 'Revisa los datos de la cuenta';
		t.WhatId = a.Id;
		Date actualDate = Date.Today();
		actualDate=actualDate.addDays(7);
		t.ActivityDate = actualDate;
		t.OwnerId = a.OwnerId;
		lstTasks.add(t);
	}
	insert lstTasks;
}