trigger t_Account on Account (after delete, after insert, after undelete, after update, 
							  before delete, before insert, before update) {
	List<Account> listNewRecords = Trigger.new; //Only available in insert and update triggers, and the records can only be modified in before triggers. 
	List<Account> listoldRecords = Trigger.old; //Only available in update and delete triggers.
	Map<ID,Account> mapNewRecords = Trigger.newMap; // Only available in before update, after insert, and after update triggers.
	Map<ID,Account> mapOldRecords = Trigger.oldMap; // Only available in update and delete triggers.
	Integer recordsSize = Trigger.size;
	
	
	if(Trigger.isBefore){ 
		// before
		if(Trigger.isInsert) { 
			//before insert
		}else if(Trigger.isUpdate){ 
			//before update
		}else if(Trigger.isDelete){ 
			//before delete
		}	
	}else{ 
		// after
		if(Trigger.isInsert) { 
			//after insert
		}else if(Trigger.isUpdate){
			//after update
		}else if(Trigger.isDelete){
			//after delete
		}else if(Trigger.isUndelete){ 
			//after undelete
		}
	}
}